#!/usr/bin/env python3
import logging
import sys
import os

import cherrypy as cp

from src.gify import generate_html

LOGGER = logging.getLogger(__name__)

API_KEY = os.environ["GIPHY_API_KEY"]

class CatServer:
    def index(self):
        return generate_html(api_key=API_KEY, tag="panda")
    index.exposed = True

    @cp.expose
    def health(self):
        return "OK"


def main():
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    cp.config.update(
        {
            'server.socket_host': '0.0.0.0',
            'server.socket_port': 5000
        }
    )
    cp.quickstart(CatServer(), '/', conf)


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    main()
