provider "aws" {
  region = "${var.aws_region}"
  profile = "quantexa"
}

resource "aws_vpc" "jenkins" {
  cidr_block = "10.100.0.0/16"
}

resource "aws_internet_gateway" "jenkins" {
  vpc_id = "${aws_vpc.jenkins.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.jenkins.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.jenkins.id}"
}

resource "aws_subnet" "jenkins" {
  vpc_id                  = "${aws_vpc.jenkins.id}"
  cidr_block              = "10.100.1.0/24"
  map_public_ip_on_launch = true
}

resource "aws_security_group" "jenkins" {
  name        = "jenkins_sg"
  description = "Allow ssh and http (80 and 8080)"
  vpc_id      = "${aws_vpc.jenkins.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "auth" {
  key_name   = "${var.key_name}"
  public_key = "${file(var.public_key_path)}"
}

resource "aws_instance" "web" {

  instance_type = "t2.micro"

  ami = "${lookup(var.aws_amis, var.aws_region)}"

  key_name = "${aws_key_pair.auth.id}"

  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.jenkins.id}"]

  subnet_id = "${aws_subnet.jenkins.id}"

}