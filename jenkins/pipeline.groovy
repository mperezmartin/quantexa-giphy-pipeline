pipeline {

    agent any

    parameters {
        booleanParam(defaultValue: false, description: '', name: 'buildAll')
    }

    environment {
        TEST = 'false'
        GIPHY_API_KEY = '<GIPHY_API_KEY>'
    }
    stages {

        stage('Debug') {
            steps {
                sh 'printenv | sort'
                script {
                    def changeLogSets = currentBuild.changeSets
                    for (int i = 0; i < changeLogSets.size(); i++) {
                        def entries = changeLogSets[i].items
                        for (int j = 0; j < entries.length; j++) {
                            def entry = entries[j]
                            echo "${entry.commitId} by ${entry.author} on ${new Date(entry.timestamp)}: ${entry.msg}"
                            def files = new ArrayList(entry.affectedFiles)
                            for (int k = 0; k < files.size(); k++) {
                                def file = files[k]
                                echo "  ${file.editType.name} ${file.path}"
                            }
                        }
                    }
                }
            }
        }

        stage('Clone repository') {
            /* Let's make sure we have the repository cloned to our workspace */
            steps {
                checkout(
                [
                    $class: 'GitSCM', 
                    branches: 
                    [
                        [
                            name: '*/master'
                        ]
                    ], 
                    doGenerateSubmoduleConfigurations: false, 
                    extensions: 
                    [
                        [
                            $class: 'WipeWorkspace'
                        ],
                        [
                            $class: 'SubmoduleOption', 
                            disableSubmodules: false, 
                            parentCredentials: true, 
                            recursiveSubmodules: true, 
                            reference: '', 
                            trackingSubmodules: true
                        ]
                    ], 
                    submoduleCfg: [], 
                    userRemoteConfigs: 
                    [
                        [
                            credentialsId: '<BITBUCKET_CREDENTIALS_ID>', 
                            url: 'git@bitbucket.org:mperezmartin/quantexa-giphy-pipeline.git'
                        ]
                    ]
                ])
            }
        }

        stage('Build & push giphy container') {
            steps {
                script {
                    docker.withRegistry("https://<DOCKER_REGISTRY_URL>", "ecr:<AWS_REGION>:<AWS_CREDENTIALS_ID>") {
                        def giphyImage = docker.build("<DOCKER_REGISTRY_URL>/giphy:0-$env.BUILD_NUMBER",
                                "-f docker/alpine.Dockerfile giphy-service")
                        giphyImage.push()
                        giphyImage.push("latest")
                    }
                }
            }
        }

        stage('Roll update') {
            steps {
                withEnv(["GIPHY_API_KEY=$env.GIPHY_API_KEY","BUILD_NUMBER=$env.BUILD_NUMBER"]) {
                    sh '''#!/bin/bash
                        /usr/local/bin/helm upgrade giphy --set apiKey=$GIPHY_API_KEY,image.tag=0-$BUILD_NUMBER \
                        --tiller-namespace giphy --namespace giphy charts-00/giphy
                    '''
                }
            }
        }
    }
    post {
        always {
            script {
                def buildResult = currentBuild.currentResult
                if ( buildResult == "SUCCESS" ) {
                    slackSend color: "good", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was successful"
                }
                else if( buildResult == "FAILURE" ) { 
                    slackSend color: "danger", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was failed"
                }
                else if( buildResult == "UNSTABLE" ) { 
                    slackSend color: "warning", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was unstable"
                }
                else {
                    slackSend color: "danger", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} its resulat was unclear"	
                }
            }
        }
    }
}