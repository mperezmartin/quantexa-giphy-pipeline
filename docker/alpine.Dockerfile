FROM alpine

WORKDIR /usr/src/app
HEALTHCHECK --interval=5s --timeout=2s CMD curl --fail http://localhost:5000/health || exit 1

COPY requirements.txt ./
RUN apk add python3 curl && pip3 install --no-cache-dir --upgrade pip && pip3 install --no-cache-dir -r requirements.txt

COPY src ./src
COPY public ./public
COPY main.py ./

ENV GIPHY_API_KEY test

EXPOSE 5000
CMD ["python3", "main.py"]